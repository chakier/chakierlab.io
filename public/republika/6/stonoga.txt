	Stonoga

Mieszka�a stonoga pod Bia�a,
Bo tak si� jej podoba�o.
Raz przychodzi li�cik ma�y
Do stonogi,
�e proszona jest do Bia�ej
Na pierogi.
Ucieszy�o to stonog�,
Wi�c ruszy�a szybko w drog�.

Nim zd��y�a doj�� do Bia�ej,
Nogi jej si� popl�ta�y:
Lewa z praw�, przednia z tyln�,
Ka�dej nodze bardzo pilno,
Sz�sta zd��y� chce za si�dm�,
Ale si�dmej i�� za trudno,
No, bo przed ni� stoi �sma,
Kt�ra w�a�nie jaki� guz ma.

Chcia�a min�� jedenast�,
Popl�ta�a si� z pi�tnast�,
A ta zn�w z dwudziest� pi�t�,
Trzydziesta z dziewi��dziesi�t�,
A druga z czterdziest� czwart�,
Cho� wcale nie by�o warto.

Stan�a stonoga w�r�d drogi,
Rozpl�ta� chce sobie nogi;
A w Bia�ej stygn� pierogi!

Rozpl�ta�a pierwsz�, drug�,
Z trzeci� trwa�o bardzo d�ugo,
Zanim dosz�a do trzydziestej,
Zapomnia�a o dwudziestej,
Przy czterdziestej ju� si� krz�ta,
No, a gdzie jest pi��dziesi�ta?
Sze��dziesi�t� nog� beszta:
Pr�dzej, pr�dzej! A gdzie reszta?

To wszystko tak d�ugo trwa�o,
�e przez ten czas ca�� Bia��
Przemalowano na zielono,
A do Zielonej stonogi nie proszono.
