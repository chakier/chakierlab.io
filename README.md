Repozytorium jest kopią strony chakier.bblog.pl pobranej z WaybackMachine

```
docker run --rm -it -v \
    $PWD:/websites hartator/wayback-machine-downloader \
    chakier.bblog.pl --to 20160416043543 -c 5
```

Materiał ma cel wyłącznie archiwalny. Wszelkie prawa pozostają przy autorach.